// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// 10.9.0
let package = Package(
    name: "FreestarAds-GMA-Dependencies",
     platforms: [
        .iOS(.v11),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FreestarAds-GMA-Dependencies",
            type: .static,
            targets: [
             "FreestarAds-GMA-Dependencies",
             "FacebookAdapter",
             "FBAudienceNetwork"
             ]
            )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(
            url: "https://github.com/googleads/swift-package-manager-google-mobile-ads.git",
            exact: "10.9.0"
            )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "FreestarAds-GMA-Dependencies",
            dependencies: [
                    .product(name: "GoogleMobileAds", package: "swift-package-manager-google-mobile-ads")
                ]
            ),
        .binaryTarget(
            name: "FacebookAdapter",
            url: "https://gitlab.com/freestar/spm-freestarads-gma-dependencies/-/raw/10.9.0/FacebookAdapter.xcframework.zip",
            checksum: "1f0846a4d1fe5b0f83f84db7ec5854d80eb8e1fd06bb8406ba94c66e9033beea"
            ),
        .binaryTarget(
            name: "FBAudienceNetwork",
            url: "https://gitlab.com/freestar/spm-freestarads-gma-dependencies/-/raw/10.9.0/FBAudienceNetwork.xcframework.zip",
            checksum: "208ec16bb88bb24fbabb512a65201550b7e3dc2cddd70e4a0417d5df8a6d2254"
            )
    ]
)
